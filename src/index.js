import React from 'react'
import { Provider } from 'react-redux'
import { NetInfo } from "react-native";

NetInfo.isConnected.addEventListener("connectionChange", console.log);

import Reactotron from 'reactotron-react-native';
Reactotron.log('hello rendering world');

if (__DEV__) {
    import('./config/reactotron').then(() => console.log('Reactotron Configured'))
}

import store from './store'

import RepositoryList from './components/RepositoryList'

const App = () => (
    <Provider store={store}>
        <RepositoryList />
    </Provider>
)

export default App