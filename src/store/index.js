import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'

import { offlineMiddleware, suspendSaga, consumeActionMiddleware } from 'redux-offline-queue'

import rootReducer from './ducks'
import rootSaga from './sagas'
import Reactotron from '../config/reactotron'

const middlewares = []
const sagaMiddleware = createSagaMiddleware()

middlewares.push(offlineMiddleware())
middlewares.push(suspendSaga(sagaMiddleware))
middlewares.push(consumeActionMiddleware())

const store = createStore(
    rootReducer,
    compose(applyMiddleware(...middlewares), Reactotron.createEnhancer())
)

sagaMiddleware.run(rootSaga)

export default store