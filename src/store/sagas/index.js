import { all, spawn, takeEvery } from 'redux-saga/effects'

import { RepositoryTypes } from '../ducks/repositories'
import { addRepository } from './repositories'

import { startWatchingNetworkConnectivity } from './offline'

export default function* rootSaga() {
    yield all([
        spawn(startWatchingNetworkConnectivity),
        takeEvery(RepositoryTypes.ADD_REPOSITORY_REQUEST, addRepository)
    ])
}  