import Reactotron from 'reactotron-react-native'
import { reactotronRedux } from 'reactotron-redux'

const reactotron = Reactotron.configure({ host: '10.10.20.215', name: 'React Native Demo' }).useReactNative().use(reactotronRedux()).connect();

export default reactotron